﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandScript : MonoBehaviour
{
    [SerializeField]
    private Transform m_helper;
    void Update()
    {
        transform.position = Vector3.Scale((Input.mousePosition), new Vector3(1, 1, 0));
        transform.LookAt(m_helper);

    }
}
