﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class clickingfix : MonoBehaviour
{
    [SerializeField] private Sprite m_Open;
    [SerializeField] private Sprite m_Closed;
    [SerializeField] private Image m_Image;


    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            m_Image.sprite = m_Closed;
        }

        else
        {
            m_Image.sprite = m_Open;
        }
    }
}
