﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SeatBehaviour : MonoBehaviour
{
    [SerializeField] private PassengerBehaviour m_Passenger = null;

    private bool m_IsTaken = false;

    public bool IsTaken
    {
        get { return m_IsTaken; }
        set { m_IsTaken = value; }
    }

    public PassengerBehaviour Passenger
    {
        get { return m_Passenger; }
        set { m_Passenger = value; }
    }

    public void ClaimSeat(PassengerBehaviour passenger)
    {
        m_Passenger = passenger;
    }
}
