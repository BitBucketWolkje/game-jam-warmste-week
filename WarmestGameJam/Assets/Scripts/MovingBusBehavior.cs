﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBusBehavior : MonoBehaviour
{
    public GameObject BusSprite;

    public GameObject StreetPrefab;
    public GameObject Streets;



    public Animator BusAnimator;
    [SerializeField]
    private int m_timeTillStop;

    private Vector3 m_moveDistance = new Vector3(Screen.width, 0, -0.1f);

    private Vector3 m_beginBus;
    public RectTransform m_endBus;

    private bool m_isStopped =false;
    private float m_distance;

    public RectTransform m_lastStreet;
    private Vector3 m_EndStreet;
    private GameObject m_newStreet;

    private BusBehaviour m_busBehaviour;

    private bool m_StreetMade = false;
    private float m_timer;
    [SerializeField]
    private float m_timeForStop;

    [SerializeField]
    private float m_speed;

    private GameObject parentStreet;


    private void Start()
    {
        parentStreet = GameObject.FindGameObjectWithTag("Street");
        m_busBehaviour = GetComponent<BusBehaviour>();
        m_beginBus = BusSprite.transform.position;
        m_EndStreet = m_lastStreet.position+m_moveDistance;
        
    }
    // Update is called once per frame
    void Update()
    {

        MoveStreet();
        
    }

    public void MoveStreet()
    {
        if (m_isStopped)
        {
            if (!m_StreetMade)
            {
                m_newStreet = Instantiate(StreetPrefab, Streets.transform);
                m_newStreet.transform.position = m_EndStreet;
                m_newStreet.transform.SetAsFirstSibling();
                
                m_StreetMade = true;
            }
            Vector3 ScreenMovement = (Camera.main.ScreenToWorldPoint(m_moveDistance)/m_timeForStop)*Time.deltaTime*m_speed;
            ScreenMovement.Scale(Vector3.right);
            Streets.transform.position -= ScreenMovement;
                BusSprite.transform.position -= ScreenMovement;
        }
    }



    public void Stop()
    {
        m_isStopped = true;//start moving street
        BusAnimator.Play("Idle");//animation
        m_timer = 0;
        MoveStreet();
    }

    public void Move()
    {
        m_distance = Vector2.Distance(m_endBus.position, m_beginBus);//calculates the distance for the speed

        BusAnimator.Play("Moving"); //woble animaation 

        BusSprite.transform.position = Vector2.MoveTowards(BusSprite.transform.position, m_endBus.position, (m_distance/m_timeTillStop)* Time.deltaTime); //moving

        m_isStopped = false; // stop moving street
        m_StreetMade = false;

    }
}
