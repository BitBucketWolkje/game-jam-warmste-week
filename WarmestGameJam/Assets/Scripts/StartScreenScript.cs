﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class StartScreenScript : MonoBehaviour
{
    public AudioSource Speaker;
    public string URL = "https://tejo.be/";
    public int LevelNumber;
    public Animator Animator;
    public Animator Bus;
    private bool _timerStarted;

    [SerializeField]
    private float _timer;

    private void Start()
    {
        BusBehaviour.m_TotalPeopleDelivered = 0;
        BusBehaviour.m_CurrentStop = 0;
        _timerStarted = false;
        Speaker = this.GetComponent<AudioSource>();
    }
    private void Update()
    {
        if(_timerStarted)
        {
            _timer += Time.deltaTime;
        }
        if (_timer > 1.5f)
        {
            LevelNumber = 1;
            SceneManager.LoadScene(LevelNumber);

        }
    }
    public void OpenURL()
    {
        Application.OpenURL(URL);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void StartGame()
    {
        _timerStarted = true;
        Bus.SetTrigger("EnterLevel");
        Animator.SetTrigger("NextLevel");

    }
    public void PlaySound(AudioClip clip)
    {

        Speaker.clip = clip;
        Speaker.Play();
    }

}
