﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassengerManagerBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject m_Passenger = null;
    private GameObject[] m_Passengers;
    [SerializeField] private GameObject m_Bus;
    private StandingAreaBehaviour m_StandingArea;
    [SerializeField] private const int m_MaxPassengerCount = 10;
    [SerializeField] private const int m_MaxNewPassengerCount = 5;
    private int m_CurrentPassengerAmount = 0;
    private int m_PassengersOnNextStop = 1;

    public StandingAreaBehaviour StandingArea
    {
        get { return m_StandingArea; }
        set { m_StandingArea = value; }
    }

    private void Awake()
    {
        m_StandingArea = m_Bus.GetComponent<StandingAreaBehaviour>();
        m_Passengers= new GameObject[m_MaxPassengerCount];
    }

    public int BusStop()
    {
        int busHappinessDecrement = 0;
        foreach (var passenger in m_Passengers)
        {
            if (passenger != null)
            {
                PassengerBehaviour passengerScript = passenger.GetComponent<PassengerBehaviour>();
                passengerScript.BusStop();
                if (passengerScript.StopsLeft <= 0)
                {
                    BusBehaviour.m_TotalPeopleDelivered++;
                    m_CurrentPassengerAmount--;
                    if (!passengerScript.GetOffBus())
                    {
                        ++busHappinessDecrement;
                    }
                }
            }
        }
        return busHappinessDecrement;
    }

    public void CreatePassenger()
    {
        for (int j = 0; j < m_PassengersOnNextStop; j++)
        {
            GameObject passenger = Instantiate(m_Passenger);
            for (int i = 0; i < m_MaxPassengerCount; ++i)
            {
               if (m_Passengers[i] == null)
               {
                   m_StandingArea.AssignSeat(passenger);
                   m_Passengers[i] = passenger;
                   ++m_CurrentPassengerAmount;
                   break;
               }
            }
        }
    }

    public void CalculatePassengersForNextStop()
    {
        if (m_MaxNewPassengerCount > m_StandingArea.CurrentPassengerAmount)
        {
            m_PassengersOnNextStop = Random.Range(1, m_MaxNewPassengerCount - m_StandingArea.CurrentPassengerAmount + 1);
        }
        else
        {
            m_PassengersOnNextStop = 0;
        }
    }
}
