﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreetBehavior : MonoBehaviour
{
    private Image m_image;
    public Sprite[] Backgrounds = new Sprite[5];
    private SpriteRenderer Renderer;
    // Start is called before the first frame update
    void Start()
    {
        m_image = GetComponent<Image>();
        
        m_image.sprite = Backgrounds[Random.Range(0, 6)];
    }

}
