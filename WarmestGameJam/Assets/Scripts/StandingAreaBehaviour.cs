﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StandingAreaBehaviour : MonoBehaviour
{

    private GameObject[] _passengersStanding;

    [SerializeField]
    private GameObject[] _locations;

    [SerializeField] private PassengerManagerBehaviour m_PassengerManager;
    private int m_CurrentPassengerAmount;
    public int m_MaxPassengerAmount = 5;

    private void Start()
    {
        _passengersStanding = new GameObject[m_MaxPassengerAmount];
    }

    public int CurrentPassengerAmount
    {
        get
        {
            int count = 0;
            foreach (var passenger in _passengersStanding)
            {
                if (passenger != null)
                {
                    count++;
                }
            }
            return count;
        }
        set { m_CurrentPassengerAmount = value; }
    }

    public void AssignSeat(GameObject passenger)
    {
        for (int i = 0; i < _passengersStanding.Length; i++)
        {
            if (_passengersStanding[i] == null)
            {
                ++m_CurrentPassengerAmount;
                _passengersStanding[i] = passenger;
                passenger.transform.position = _locations[i].transform.position;
                break;
            }
        }
    }

    public void StandingAreaSwitchout(GameObject CurrentStandingPerson, GameObject CurrentSittingPerson = null)
    {
        if (CurrentSittingPerson == null)
        {
            --m_CurrentPassengerAmount;
        }
        for (int i = 0; i < _passengersStanding.Length; i++)
        {
            if(_passengersStanding[i] == CurrentStandingPerson)
            {
                _passengersStanding[i] = CurrentSittingPerson;
                CurrentStandingPerson.GetComponent<PassengerBehaviour>().SwitchSprite();
                if (CurrentSittingPerson != null)
                {
                    CurrentSittingPerson.GetComponent<PassengerBehaviour>().SwitchSprite();
                }
                return;
            }
        }
    }
}