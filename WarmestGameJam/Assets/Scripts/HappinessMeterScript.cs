﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class HappinessMeterScript : MonoBehaviour
{
    [SerializeField]
    private BusBehaviour _busBehaviour;

    
    // Update is called once per frame
    void Update()
    {
        if(_busBehaviour != null)
        {
            this.GetComponent<Image>().fillAmount = _busBehaviour.GetHappinessPercentage();

        }

        if (_busBehaviour.GetHappinessPercentage() <= 0)
        {
            Cursor.visible = true;
            SceneManager.LoadScene(2);
        }
    }
}
