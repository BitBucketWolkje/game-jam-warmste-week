﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgeScript : MonoBehaviour
{
    public Text _ageText;
    public PassengerBehaviour _passengerBehaviour;

    private void Start()
    {
        if (this.GetComponent<PassengerBehaviour>() != null)
        {
            _passengerBehaviour = this.GetComponent<PassengerBehaviour>();
        }

    }
    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<PassengerBehaviour>() != null)
        {
            _ageText.text = _passengerBehaviour.Age.ToString() + " years";

        }
    }
}
