﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PeopleTextScript : MonoBehaviour
{
    public string PrefixedPeopleText;
    public string SuffixedPeopleText;
    public int PeopleNumber;

    // Start is called before the first frame update
    void Start()
    {
        PeopleNumber = BusBehaviour.m_TotalPeopleDelivered;
       this.gameObject.GetComponent<Text>().text = PrefixedPeopleText + PeopleNumber + SuffixedPeopleText;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
