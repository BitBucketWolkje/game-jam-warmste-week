﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopsUIScript : MonoBehaviour
{
    public Image StopsBar;
    public GameObject StopsBarPanel;
    public PassengerBehaviour PassengerBehaviour;
    
    public List<Image> StopsBlocksArray;

    public bool HasRemovedBar;

    private void Start()
    {
        Image CurrentBlock;
        PassengerBehaviour = this.gameObject.GetComponent<PassengerBehaviour>();
        for(int i = 0; i < PassengerBehaviour.StopsLeft; i++)
        {
            CurrentBlock = Instantiate(StopsBar, StopsBarPanel.transform);
            CurrentBlock.transform.position += new Vector3(i * 10, 0);
            StopsBlocksArray.Add(CurrentBlock);
        }
    }
    private void Update()
    {
        Image CurrentBlock;
        if (PassengerBehaviour.IsHappy)
        {
            foreach(Image image in StopsBlocksArray)
            {
                image.color = Color.white;
            }
        }
        if (!PassengerBehaviour.IsHappy)
        {
            foreach (Image image in StopsBlocksArray)
            {
                image.color = Color.red;
            }
        }
        if(HasRemovedBar == false)
        {
            CurrentBlock = StopsBlocksArray[PassengerBehaviour.StopsLeft];
            Destroy(CurrentBlock);

            StopsBlocksArray.RemoveAt(PassengerBehaviour.StopsLeft);
            HasRemovedBar = true;
        }


    }
}
