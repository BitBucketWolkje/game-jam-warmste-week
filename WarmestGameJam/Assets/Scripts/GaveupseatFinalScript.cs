﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GaveupseatFinalScript : MonoBehaviour
{
    public string PrefixedPeopleText;
    public string SuffixedPeopleText;
    public int PeopleNumber;
    [SerializeField]
    private Text _text;

    [SerializeField]
    private int _timerUntillAppearing;
    [SerializeField]
    private int _appearingSpeed;

    private bool _colorHasChanged = false;

    public Color oldColor;
    public Color newColor;

    [SerializeField]
    private GameObject textobject;

    private float _timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
        _text.text = PassengerBehaviour._peopleWhoGaveUpTheirSeats.ToString() + " people gave up their seat for someone older.";
        _text.color = oldColor;
    }
    
    private void Update()
    {
        if(_timer < _timerUntillAppearing)
        _timer += Time.deltaTime;
        else if(_timer > _timerUntillAppearing && !_colorHasChanged)
        {
            _text.color = Color.Lerp(oldColor, newColor, _appearingSpeed);
            
        }

        if(_text.color == newColor && !_colorHasChanged)
        {
            _timer = 0;
            _timerUntillAppearing = 2;
            _timer += Time.deltaTime;
            _colorHasChanged = true;
        }
        if (_colorHasChanged && _timer > _timerUntillAppearing)
        {
            textobject.SetActive(true);
            Destroy(this);
        }
    }
}
