﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PassengerBehaviour : MonoBehaviour
{
    [SerializeField] private StandingAreaBehaviour m_StandingArea = null;
    [SerializeField] private SeatBehaviour m_Seat = null;
    [SerializeField] private int m_Age;
    [SerializeField] private int m_StopsLeft;

    [SerializeField] private const int m_MinAge = 7;
    [SerializeField] private const int m_MaxAge = 95;
    [SerializeField] private const int m_MaxStops = 5;

    [SerializeField] private  Sprite m_boy_sitting;
    [SerializeField] private Sprite m_boy_standing;
    [SerializeField] private Sprite m_girl_sitting;
    [SerializeField] private Sprite m_girl_standing;

    [SerializeField] private Sprite m_man_sitting;
    [SerializeField] private Sprite m_man_standing;
    [SerializeField] private Sprite m_woman_sitting;
    [SerializeField] private Sprite m_woman_standing;

    [SerializeField] private Sprite m_oldman_sitting;
    [SerializeField] private Sprite m_oldman_standing;
    [SerializeField] private Sprite m_oldwoman_sitting;
    [SerializeField] private Sprite m_oldwoman_standing;
    [SerializeField] private SpriteRenderer m_Sprit;

    [SerializeField] private StopsUIScript _stopsUIScript;

    private Sprite m_sittingSprite;
    private Sprite m_standingSprite;

    private SpriteRenderer m_ImageRenderer;
    private Vector3 m_PreviousPosition;//position when you start dragging
    private Vector3 m_ScreenPoint;
    private Vector3 m_RelativePosition; //position relative to mouse
    private float m_FadeTime = 0.0f;
    private const float m_MaxFadeTime = 1.0f;
    private bool m_IsGettingOff = false;
    private bool m_IsHappy;
    private bool m_IsSitting;
    private bool m_IsDragged;

    private Vector3 _standingScale = new Vector3(1.2f, 1.2f, 1.2f);
    private Vector3 _sittingScale;



    static public int _peopleWhoGaveUpTheirSeats = 0;

    private void Start()
    {
        Assign_sprites();

        m_Sprit.sprite = m_standingSprite;
        transform.localScale = _standingScale;
    }

    private void Assign_sprites()
    {
        if (UnityEngine.Random.Range(0,2) == 0)
        {
            CreateBoy();
        }
        else
        {
            CreateGirl();
        }
    }

    private void CreateGirl()
    {
        if (Age < 20)
        {
            m_sittingSprite = m_girl_sitting;
            m_standingSprite = m_girl_standing;
        }
        else if (Age > 50)
        {
            m_sittingSprite = m_oldwoman_sitting;
            m_standingSprite = m_oldwoman_standing;
        }
        else
        {
            m_sittingSprite = m_woman_sitting;
            m_standingSprite = m_woman_standing;

        }
    }

    private void CreateBoy()
    {
        if(Age < 20)
        {
            m_sittingSprite = m_boy_sitting;
            m_standingSprite = m_boy_standing;
        }
        else if (Age > 50)
        {
            m_sittingSprite = m_oldman_sitting;
            m_standingSprite = m_oldman_standing;
        }
        else
        {
            m_sittingSprite = m_man_sitting;
            m_standingSprite = m_man_standing;

        }
    }

    public int Age
    {
        get { return m_Age; }
        set { m_Age = value; }
    }

    public int StopsLeft
    {
        get { return m_StopsLeft; }
        set { m_StopsLeft = value; }
    }

    public bool IsHappy
    {
        get { return m_IsHappy; }
        set { m_IsHappy = value; }
    }

    private void Awake()
    {
        _sittingScale = transform.localScale;
        m_ImageRenderer = GetComponentInChildren<SpriteRenderer>();
        m_ImageRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        m_StandingArea = GameObject.Find("Bus").GetComponent<StandingAreaBehaviour>();
        m_Age = UnityEngine.Random.Range(m_MinAge, m_MaxAge);
        m_StopsLeft = UnityEngine.Random.Range(2, m_MaxStops);
        _stopsUIScript = this.gameObject.GetComponent<StopsUIScript>();
        
    }

    private void Update()
    {
        if (m_FadeTime < m_MaxFadeTime && !m_IsGettingOff)
        {
            m_FadeTime += Time.deltaTime;
            m_ImageRenderer.color = new Color(1.0f,1.0f,1.0f, m_FadeTime);
        }

        if (m_IsGettingOff)
        {
            if (m_Seat != null)
            {
                m_Seat.IsTaken = false;
                m_Seat.Passenger = null;
            }
            Invoke("Kill", m_MaxFadeTime);
            m_FadeTime -= Time.deltaTime;
            m_ImageRenderer.color = new Color(1.0f, 1.0f, 1.0f, m_FadeTime);
        }


    }

    private void OnMouseDown()
    {
        m_PreviousPosition = transform.position;
        m_RelativePosition = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_IsDragged = true;
    }

    private void OnMouseDrag()
    {
        if (m_IsDragged)
        {
            Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, m_ScreenPoint.z);
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + m_RelativePosition;
            transform.position = currentPosition;
        }
    }

    private void OnMouseUp()
    {
        m_IsDragged = false;
        if (m_Seat != null)
        {
            if (m_Seat.Passenger == null)
            {
                m_IsSitting = true;
                m_Seat.IsTaken = true;
                m_Seat.Passenger = this;
                transform.position = m_Seat.transform.position;
                m_StandingArea.StandingAreaSwitchout(this.gameObject);
                return;
            }
            else
            {
                if (m_Seat.Passenger.Age < m_Age)
                {
                    m_StandingArea.StandingAreaSwitchout(this.gameObject, m_Seat.Passenger.gameObject);
                    m_Seat.Passenger.transform.position = m_PreviousPosition;
                    m_Seat.Passenger.m_IsSitting = false;
                    m_Seat.Passenger.m_Seat = null;
                    m_Seat.Passenger = this;

                    m_IsSitting = true;
                    transform.position = m_Seat.transform.position;
                    _peopleWhoGaveUpTheirSeats++;
                    return;
                }
            }
        }
        transform.position = m_PreviousPosition;
        m_Seat = null;
    }

    public void SwitchSprite()
    {
        if(m_Sprit.sprite == m_sittingSprite)
        {
            m_Sprit.sprite = m_standingSprite;
            transform.localScale = _standingScale;
        }
        else if (m_Sprit.sprite == m_standingSprite)
        {
            transform.localScale = _sittingScale;
            m_Sprit.sprite = m_sittingSprite;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Seat")
        {
            m_Seat = other.gameObject.GetComponent<SeatBehaviour>();
        }
    }

    public void BusStop()
    {
        --m_StopsLeft;
        _stopsUIScript.HasRemovedBar = false;
        if (m_IsSitting)
        {
            m_IsHappy = true;
        }
    }

    public bool GetOffBus()
    {
        if (m_Seat != null)
        {
            m_Seat.Passenger = null;
            m_Seat.IsTaken = false;
        }
        m_IsGettingOff = true;
        return m_IsHappy;
    }

    public void Kill()
    {
        Destroy(this.gameObject);
    }
}
