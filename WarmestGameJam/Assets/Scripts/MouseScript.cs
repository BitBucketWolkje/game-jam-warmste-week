﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScript : MonoBehaviour
{
    [SerializeField]
    private Transform m_helper;

    private void Start()
    {
    Cursor.visible = false;

    }
    void Update()
    {
        transform.position = Vector3.Scale(  (Input.mousePosition) , new Vector3(1, 1, 0));
        transform.LookAt(m_helper);

    }
}
