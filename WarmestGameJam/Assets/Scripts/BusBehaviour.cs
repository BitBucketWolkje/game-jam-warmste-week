﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


public class BusBehaviour : MonoBehaviour
{
    public bool m_HasReachedStop;
    static public int m_CurrentStop = 0;
    private int m_CurrentBusHappiness;

    static public int m_TotalPeopleDelivered;

    private List<PassengerBehaviour> m_Passengers=new List<PassengerBehaviour>();

    [SerializeField] private GameObject m_PassengerManager;
    [SerializeField] private int m_MaxBusHappiness = 100;
    [SerializeField] private const int m_SeatAmount=5;
    [SerializeField] private float m_SeatDistance = 5.0f;
    [SerializeField] private float m_TimeNeededForAStop = 10;
    [SerializeField] private StandingAreaBehaviour m_StandingAreaBehaviour;
    [SerializeField]private float m_TimeNeededStandingStill;
    private float m_TimeStandingStill;


    private bool m_StandingStill = false;


    public float m_TimeTillNextStop;

    private MovingBusBehavior m_movingBus;

    [SerializeField]
    private GameObject[] m_Seats;
    
    // Start is called before the first frame update

    private void Awake()
    {
        m_CurrentBusHappiness = m_MaxBusHappiness;
        m_movingBus = GetComponent<MovingBusBehavior>();
        m_StandingAreaBehaviour = GetComponent<StandingAreaBehaviour>();
        m_TimeTillNextStop = m_TimeNeededForAStop;
    }
    void Update()
    {


        if (!m_HasReachedStop && m_TimeTillNextStop >= 0&&!m_StandingStill)
        {
            m_TimeTillNextStop -= Time.deltaTime;
            m_movingBus.Move();
        }

        else if (m_TimeTillNextStop < 0 && !m_HasReachedStop)
        {
            m_HasReachedStop = true;
            m_TimeTillNextStop = m_TimeNeededForAStop;
        }

        else if (m_HasReachedStop)
        { 
            m_CurrentBusHappiness -= m_PassengerManager.GetComponent<PassengerManagerBehaviour>().BusStop();
            Debug.Log("Current Bus Happiness is: " + m_CurrentBusHappiness);
            m_CurrentStop += 1;
            m_TimeStandingStill = 0;
            m_StandingStill = true;
            m_HasReachedStop = false;
            m_PassengerManager.GetComponent<PassengerManagerBehaviour>().CalculatePassengersForNextStop();
            m_PassengerManager.GetComponent<PassengerManagerBehaviour>().CreatePassenger();
        }
        if (m_StandingStill)
        {
            m_TimeStandingStill += Time.deltaTime;
            m_movingBus.Stop();
            if (m_TimeStandingStill > m_TimeNeededStandingStill)
            {
                m_StandingStill = false;
            }
        }
    }

    public float GetHappinessPercentage()
    {
        return m_CurrentBusHappiness / (float)m_MaxBusHappiness;
    }
}