﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopsTextScript : MonoBehaviour
{
    public string PrefixedStopsText;
    public string SuffixedStopsText;

    public int StopsNumber;



    // Start is called before the first frame update
    void Start()
    {
        StopsNumber = BusBehaviour.m_CurrentStop;
        this.gameObject.GetComponent<Text>().text = PrefixedStopsText + StopsNumber + SuffixedStopsText;
            



    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
