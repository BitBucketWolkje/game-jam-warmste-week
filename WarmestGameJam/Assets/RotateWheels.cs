﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWheels : MonoBehaviour
{
    private int _speed = 50;
    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(new Vector3(0, 0, Time.deltaTime * _speed));
    }
}
