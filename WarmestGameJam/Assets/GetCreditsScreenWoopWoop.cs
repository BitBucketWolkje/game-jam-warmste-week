﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GetCreditsScreenWoopWoop : MonoBehaviour
{
    [SerializeField] private float _timer;
    [SerializeField] private float _timeForEnd = 4;

    public Animator Animator;
    public Animator Bus;

    private bool _timerStarted;
    private float _animationTimer;

    void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > _timeForEnd)
        {
            LoadCredits();
        }

        if (_timerStarted)
        {
            _animationTimer += Time.deltaTime;
        }
        if (_timer > 1.5f)
        {
            SceneManager.LoadScene(3);

        }
    }

    private void LoadCredits()
    {
        _timerStarted = true;

        Bus.SetTrigger("EnterLevel");
        Animator.SetTrigger("NextLevel");
    }
}
